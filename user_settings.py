#!/usr/bin/python

# (module of) PyDict Bangla
# Author: Sadman Muhib Samyo

# User Settings #

import os, shelve

configFilePath = "./config.dat"

def init_config():
	if not os.path.exists(configFilePath):
		print "init_config() executed"
		db = shelve.open(configFilePath, writeback=True)
		try:
			db['clip'] = True
			db['top'] = False
		finally:
			db.close()


def Query(data):
	db = shelve.open(configFilePath)
	Data = db[data]
	db.close()
	return Data
	
			
def Get(data):
	init_config()
	data = Query(data)
	return data


def Set(setting, data):
	init_config()
	db = shelve.open(configFilePath, writeback=True)
	try:
		db[setting] = data
	finally:
		db.close()
