PyDict Bangla is a graphical English-Bangla dictionary for linux, written in Python.
Also works in Windows platform

## Features ##
* Over 96,000+ words with antonyms
* Clipboard monitoring feature
* Simple and friendly UI, optimized for easy access

## Usage ##
Each linux os should have Python bundled with it. All you need is the extra wxPython 
library. So,

* Install wxPython: for Ubuntu run in terminal sudo apt-get install python-wxgtk2.8. If
you want to install wxPython 3, you can follow a guide from http://goo.gl/926S5M, although I haven't tested it.

* After installing wxPython, you can execute the program by running main.py from terminal

## Contributions ##
I want to add word suggestion and bookmarking feature, if you can do this for me, then most welcome. Otherwise,
I want to keep things simple and don't want extra modifications