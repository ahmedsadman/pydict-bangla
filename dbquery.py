#!/usr/bin/python

# (module of) PyDict Bangla
# Author: Sadman Muhib Samyo

# dbquery.py #
# Query the word database  #

import os
import sqlite3 as sq
from threading import Thread

__metaclass__ = type 

def Query(userinput):
	# please don't use my database without permission
	db = "./resources/words.db"
	assert os.path.exists(db), "Database not found"

	with sq.connect(db) as conn:
		try:
			cursor = conn.cursor()
			cursor.execute('select eng, bn, antonym from words where eng = "%s"' % userinput.upper() )
			eng, bn, antonym = cursor.fetchone() # normally, database will hold only one record for each word
			return (bn, antonym.lower() )
		except TypeError:
			return (None, None)


class MainProcess(Thread):
	# The Database is huge. So threading must be used to avoid UI unresponsiveness
	def __init__(self, userinput, field1, field2):
		super(MainProcess, self).__init__()
		self.userinput = userinput
		self.field1 = field1 # meaning field
		self.field2 = field2 # antonym field

	def run(self):
		meaning, antonym = Query(self.userinput)
		# the following two lines are written to prevent a mysterious bug,
		# -the bug is, sometimes values are not written when you search for words
		# -in the dictionary one after another. Maybe it's wx version or os specific
		self.field1.SetValue("")
		self.field2.SetValue("")
		# ###

		self.field1.SetValue(bool(meaning) and meaning or 'Not Found')
		self.field2.SetValue(bool(antonym) and antonym or '')