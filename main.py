#!/usr/bin/python

# PyDict Bangla
# Author: Sadman Muhib Samyo

# MAIN #
# The main module for PyDict Bangla #

import wx, os
import user_settings, dbquery, pyperclip
from wx.lib.wordwrap import wordwrap

# modern style classes
__metaclass__ = type


class StaticText(wx.StaticText):
	def __init__(self, parent, id, label, fontSize=12, weight=wx.BOLD):
		super(StaticText, self).__init__(parent, id, label)
		self.Create(parent, id, label, fontSize, weight)
		
	def Create(self, parent, id, label, fontSize, weight):
		font = wx.Font(pointSize = fontSize, family = wx.DEFAULT, style = wx.NORMAL, weight = weight)
		self.SetFont(font)


class MainUI(wx.Frame):	
	def __init__(self, *args, **kwargs):
		super(MainUI, self).__init__(*args, **kwargs)
		self.InitUI()
		
		
	def CreateMenuBar(self):
		menubar = wx.MenuBar()
		
		# Preferences Menu
		prefMenu = wx.Menu()
		self.menu_clipboard = wx.MenuItem(prefMenu, wx.ID_ANY, 'Monitor Clipboard')
		self.menu_clipboard.SetCheckable(True)
		self.menu_top = wx.MenuItem(prefMenu, wx.ID_ANY, 'Always on top')
		self.menu_top.SetCheckable(True)
		
		prefMenu.AppendItem(self.menu_clipboard)
		prefMenu.AppendItem(self.menu_top)
		# prefMenu.AppendItem(self.menu_font)
		
		menubar.Append(prefMenu, 'Preferences')
		
		# shows feature list at first run
		if not os.path.exists('./config.dat'): 
			print 'First run'
			self.ShowHelp(None)

		# remember user settings
		self.menu_clipboard.Check(user_settings.Get('clip'))
		self.menu_top.Check(user_settings.Get('top'))
		
		# Help Menu
		aboutMenu = wx.Menu()
		self.menu_help = wx.MenuItem(aboutMenu, wx.ID_ANY, 'Help')
		self.menu_about = wx.MenuItem(aboutMenu, wx.ID_ANY, 'About')
		
		aboutMenu.AppendItem(self.menu_help)
		aboutMenu.AppendItem(self.menu_about)
		
		menubar.Append(aboutMenu, 'Info')
		self.SetMenuBar(menubar)
		
		
	def InitUI(self):
		# create menubar
		self.CreateMenuBar()
		self.panel = wx.Panel(self)
		self.panel.SetBackgroundColour("white")

		# Display Texts
		self.st_meaning = StaticText(self.panel, wx.ID_ANY, 'Meaning')
		self.st_antonym = StaticText(self.panel, wx.ID_ANY, 'Antonym')
		
		# Output fields
		self.meaningField = wx.TextCtrl(self.panel, size = (180, -1), style = wx.TE_CENTER | wx.TE_READONLY | wx.NO_BORDER)
		self.antonymField = wx.TextCtrl(self.panel, size = (180, -1), style = wx.TE_CENTER | wx.TE_READONLY | wx.NO_BORDER)
		self.searchField = wx.TextCtrl(self.panel, size = (180, -1), style= wx.TE_CENTER)
		self.meaningField.SetBackgroundColour('#FFFFFF')
		self.antonymField.SetBackgroundColour('#FFFFFF')
		
		# use larger font size/siyam rupali
		largeFont = wx.Font(pointSize=14, family=wx.DEFAULT, style=wx.NORMAL, weight=wx.NORMAL)
		font_textfield = wx.Font(pointSize = 12, family = wx.DEFAULT, faceName = "Siyam Rupali", style = wx.NORMAL, weight = wx.NORMAL)
		self.antonymField.SetFont(largeFont)
		self.meaningField.SetFont(font_textfield)

		self.btn = wx.Button(self.panel, wx.ID_ANY, 'Find')
		self.container = wx.BoxSizer(wx.VERTICAL)
		
		BorderSize = 10
		self.container.Add((-1, 20))
		self.container.Add(self.st_meaning, flag = wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT, border = BorderSize)
		
		self.container.Add(self.meaningField, flag = wx.ALIGN_CENTER)
		self.container.Add((-1, 10))
		
		self.container.Add(self.st_antonym, flag = wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT, border = BorderSize)
		self.container.Add(self.antonymField, flag = wx.LEFT | wx.RIGHT, border = BorderSize)
		self.container.Add((-1, 20))
		
		self.container.Add(self.searchField, flag = wx.LEFT | wx.RIGHT, border = BorderSize)
		self.container.Add((-1, 6))
		self.container.Add(self.btn, flag = wx.ALIGN_CENTER)
		self.container.Add((-1, 10))
		
		self.panel.SetSizer(self.container)
		self.container.Fit(self)

		# Event Bindings
		self.Bind(wx.EVT_BUTTON, self.TrigFind, self.btn)
		self.Bind(wx.EVT_ACTIVATE, self.ProcessClip)
		self.Bind(wx.EVT_MENU, self.TrigFind, id=5) # 'id' used to assign shortcut later, see acceltable below.
		self.Bind(wx.EVT_MENU, lambda evt, pref='clip' : self.SetUserSettings(evt, pref), self.menu_clipboard)
		self.Bind(wx.EVT_MENU, lambda evt, pref='top' : self.SetUserSettings(evt, pref), self.menu_top)
		self.Bind(wx.EVT_MENU, self.ShowHelp, self.menu_help)
		self.Bind(wx.EVT_MENU, self.AboutBox, self.menu_about)
		self.Bind(wx.EVT_MENU, self.OnTop, self.menu_top)
	
		# this didn't work for shortcut assignment: self.btn.Bind(wx.EVT_COMMAND_ENTER, self.TrigFind), acceltable used as fallback
		# assings keyboard shortcut (press ENTER key to find an specified word) 
		self.tbl = wx.AcceleratorTable([(wx.ACCEL_NORMAL, ord('\r'), 5)]) # \r = ENTER KEY (linux only, for windows replace with \n)
		self.SetAcceleratorTable(self.tbl)

		if user_settings.Get('top') : self.ToggleWindowStyle(wx.STAY_ON_TOP) # sets always on top functionality based on user's choice

		# App Settings
		self.searchField.SetFocus()
		self.SetTitle('PyDict Bangla')
		self.Centre()
		self.Show(True)

	def SetUserSettings(self, e, pref): # pref defines which settings were clicked, 'clip' for clipboard and 'top' for always on top
		obj = e.GetEventObject()
		status = obj.IsChecked(e.GetId() ) # Id must be specified when accessing via evtObj
		user_settings.Set(pref, status)
		e.Skip() # look for other event handlers for the same event, in this case self.OnTop

	def ProcessClip(self, e):
		if e.GetActive() and self.menu_clipboard.IsChecked(): # e.GetActive() returns whether window lost (false) or gained (true) focus
			try:
				self.searchField.SetValue(pyperclip.paste() )
				self.searchField.GetValue()
				self.TrigFind(None)
				self.searchField.SetInsertionPoint(len(pyperclip.paste()) ) # set the text cursor at end of word
			except: pass
		elif e.GetActive() and not self.menu_clipboard.IsChecked():
			self.searchField.SetValue('')
			self.searchField.SetFocus()
		else: pass


	def OnTop(self, e):
		self.ToggleWindowStyle(wx.STAY_ON_TOP)
		e.Skip() # look for other event handlers for the same event, in this case self.SetUserSettings


	def TrigFind(self, e):
		userinput = self.searchField.GetValue().strip()
		if userinput != '':
			thread = dbquery.MainProcess(userinput, self.meaningField, self.antonymField)
			thread.start()


	def ShowHelp(self, e):
		text = """Clipboard Monitoring: \nIf enabled, it will monitor clipboard activities, just copy any word from anywhere and focus the application window, you will see the meaning.\nYou can disable this in Preferences"""
		wx.MessageBox(wordwrap(text, 300, wx.ClientDC(self)), 'Help', wx.OK)


	def AboutBox(self, e):
		description = 'The first graphical Eng-Ban dictionary which works in linux and windows, crafted by a hobbyist programmer'
		license = 'Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0'
		about = wx.AboutDialogInfo()
		about.SetName('PyDict Bangla')
		about.SetVersion('1.0')
		about.SetDescription(wordwrap(description, 320, wx.ClientDC(self)))
		about.SetCopyright( '(C) - Sadman Muhib Samyo - 2014')
		about.SetLicense(wordwrap(license, 320, wx.ClientDC(self)))
		about.AddDeveloper('Sadman Muhib Samyo \n(ahmedsadman.211@gmail.com)')
		wx.AboutBox(about)


def main():
	App = wx.App()
	MainUI(None, style = wx.MINIMIZE_BOX | wx.CLOSE_BOX | wx.CAPTION)
	App.MainLoop()
	
if __name__ == '__main__':
	main()
